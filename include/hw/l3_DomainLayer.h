#ifndef LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
#define LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
#include "hw/l4_InfrastructureLayer.h"

const int MIN_ID = 0;
const int MIN_ID_CLIENT = 0;
const int MIN_COEFF = 0;
const int MAX_COEFF = 100;
const int MIN_SUM = 0;
const int MAX_SUM = 10000000;

class BetCollection : public ICollectable {
 protected:
  bool invariant() const;
 public:
  BetCollection() = delete;
  BetCollection(const BetCollection& p) = delete;
  BetCollection& operator=(const BetCollection& p) = delete;
  BetCollection(int id, int id_client, float coefficient, int sum);
  bool write(std::ostream& os) override;
  int getId() const;
  int getIdClient() const;
  float getCoefficient() const;
  int getSum() const;
 private:
  int _id;
  int _id_client;
  float _coefficient;
  int _sum;
};

class ItemCollector: public ACollector {
 public:
  virtual std::shared_ptr<ICollectable> read(std::istream& is) override;
};

#endif //LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
