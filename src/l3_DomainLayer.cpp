#include "hw/l3_DomainLayer.h"

bool BetCollection::invariant() const {
  return _id > MIN_ID && _id_client > MIN_ID_CLIENT && _coefficient > MIN_COEFF && _coefficient <= MAX_COEFF && _sum > MIN_SUM && _sum <= MAX_SUM;
}
bool BetCollection::write(std::ostream& os) {
  writeNumber(os, _id);
  writeNumber(os, _id_client);
  writeNumber(os, _coefficient);
  writeNumber(os, _sum);
  return os.good();
}
int BetCollection::getId() const {
  return _id;
}
int BetCollection::getIdClient() const {
  return _id_client;
}
float BetCollection::getCoefficient() const {
  return _coefficient;
}
int BetCollection::getSum() const {
  return _sum;
}
BetCollection::BetCollection(int id, int id_client, float coefficient, int sum): _id(id), _id_client(id_client), _coefficient(coefficient), _sum(sum) {
  assert(invariant());
}
std::shared_ptr<ICollectable> ItemCollector::read(std::istream &is) {
  int id = readNumber<int>(is);
  int id_client = readNumber<int>(is);
  int coefficient = readNumber<float>(is);
  int sum = readNumber<int>(is);
  return std::make_shared<BetCollection>(id, id_client, coefficient, sum);
}
